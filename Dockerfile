FROM python:3.9-alpine AS build-env
LABEL maintainer="ema.rainho@ua.pt"

ENV APP_USER="appuser"
ENV APP_GROUP="appgroup"
ENV APP_LOCATION="/usr/src/app"
ENV CONFIG_FOLDER="private_config"
ENV PATH="$APP_LOCATION/.local/bin:$PATH"
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN mkdir -p $APP_LOCATION/$CONFIG_FOLDER && \
    addgroup --gid 9999 $APP_GROUP && \
    adduser --uid 9999 -D -G $APP_GROUP -h $APP_LOCATION $APP_USER && \
    apk add --no-cache --update rust gcc musl-dev python3-dev libffi-dev openssl-dev cargo && \
    python3 -m pip install --no-cache-dir --upgrade pip setuptools wheel pip-autoremove
COPY requirements.txt /tmp
COPY ./api_backend/ $APP_LOCATION/api_backend/
COPY ./db/ $APP_LOCATION/db/
COPY ./docker_connector/ $APP_LOCATION/docker_connector/
COPY ./samples/secrets.py.sample $APP_LOCATION/$CONFIG_FOLDER/secrets.py
COPY ./samples/python-gitlab.cfg.sample $APP_LOCATION/$CONFIG_FOLDER/python-gitlab.cfg
COPY ./samples/variables.py.sample $APP_LOCATION/$CONFIG_FOLDER/variables.py
COPY ./samples/Security-Analysis.gitlab-ci.yml $APP_LOCATION/$CONFIG_FOLDER/Security-Analysis.gitlab-ci.yml
COPY ./swagger_server/ $APP_LOCATION/swagger_server/
RUN chown $APP_USER:$APP_GROUP -R $APP_LOCATION/
WORKDIR $APP_LOCATION
USER $APP_USER
RUN python3 -m pip install --user --no-cache-dir safety && \
    python3 -m pip install --user --no-cache-dir --upgrade -r /tmp/requirements.txt && \
    if ! python3 -m pip freeze | safety check --stdin; then exit; fi && \
    pip-autoremove -y safety

FROM python:3.9-alpine
ENV PORT_NUMBER 5555
HEALTHCHECK --interval=5m --timeout=3s \
  CMD wget -nv -t1 --spider --no-check-certificate https://localhost:$PORT_NUMBER/ema.rainho/secureapps-ci/v1/health || exit 1
ENV PATH=$APP_LOCATION/.local/bin:$PATH
ENV APP_USER="appuser"
ENV APP_GROUP="appgroup"
ENV APP_LOCATION="/usr/src/app"
ENV CONFIG_FOLDER="private_config"
ENV PATH="$APP_LOCATION/.local/bin:$PATH"
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
RUN mkdir -p $APP_LOCATION && \
    addgroup --gid 9999 $APP_GROUP && \
    adduser --uid 9999 -D -G $APP_GROUP -h $APP_LOCATION $APP_USER && \
    apk add --no-cache --update git openssh-client && \
    python3 -m pip list --outdated || exit && \
    python3 -m pip uninstall -y pip
COPY --chown=$APP_USER:$APP_GROUP --from=build-env $APP_LOCATION/$CONFIG_FOLDER/ $APP_LOCATION/$CONFIG_FOLDER/
COPY --chown=$APP_USER:$APP_GROUP --from=build-env $APP_LOCATION/.local $APP_LOCATION/.local
COPY --chown=$APP_USER:$APP_GROUP --from=build-env $APP_LOCATION/api_backend/ $APP_LOCATION/api_backend/
COPY --chown=$APP_USER:$APP_GROUP --from=build-env $APP_LOCATION/db/ $APP_LOCATION/db/
COPY --chown=$APP_USER:$APP_GROUP --from=build-env $APP_LOCATION/docker_connector/ $APP_LOCATION/docker_connector/
COPY --chown=$APP_USER:$APP_GROUP --from=build-env $APP_LOCATION/swagger_server/ $APP_LOCATION/swagger_server/
WORKDIR $APP_LOCATION
USER $APP_USER
EXPOSE $PORT_NUMBER
ENTRYPOINT ["python3"]
CMD ["-m", "swagger_server"]
