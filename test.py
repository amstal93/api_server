import logging
from os.path import isdir

from git import Repo
from os import path, makedirs
from shutil import copy as shutil_copy
from api_backend.gitlab_connector import GitlabConnector
from private_config.variables import sec_analysis_yaml, gitlab_id, gitlab_cfg, gitlab_user, analysis_path, samples_path, app_name

if __name__ == '__main__':

    app_to_analyse = app_name
    file_name = f"{analysis_path}/{app_to_analyse}/{sec_analysis_yaml}"
    print("file_name: {file}".format(file=file_name))

    app_name = None
    group_name = None
    subgroup_name = None
    git_url = None
    branch_name = None
    git_depth = None
    stages = None

    gitlab_action = GitlabConnector(gitlab_id, gitlab_cfg, file_name)

    # create analysis folder
    the_folder = "{path}/{folder_name}".format(path=analysis_path, folder_name=app_to_analyse)
    if isdir(the_folder) is False:
        try:
            makedirs(the_folder, 0o755)
        except OSError:
            print("Creation of the directory {folder} failed".format(folder=the_folder))

    # copy security analysis definition
    sample_file = f"{samples_path}/{sec_analysis_yaml}"
    target_folder = f"{analysis_path}/{app_to_analyse}"
    target_file = f"{target_folder}/{sec_analysis_yaml}"

    if path.exists(target_file) is False:
        shutil_copy(sample_file, target_folder)
    else:
        print("file exists {file_name}".format(file_name=sample_file))

    # GitLab CI Lint
    # lint - analysis definition file (yaml)
    ci_definition = gitlab_action.open_ci_definition(file_name, 'dict')
    success, errors = gitlab_action.lint_yaml(ci_definition)
    print(success, errors)

    # Check Application Name
    app_name = gitlab_action.get_app_name()
    print("app_name: {}".format(app_name))

    # Check WorkGroup Name
    # Groups at GitLab is a similar concept to Units/Divisions/Sections/Departments/Teams/WorkGroups/WorkProjects
    # - Gitlab 'project' equals to a code repository
    # - GitLab 'group' contains several code repositories
    group_name = gitlab_action.get_group_name()
    workgroup_name = workgroup_path = "analysis"
    project_name = "{group}__{app}".format(group=group_name, app=app_name)
    print("workgroup_name: {workgroup}".format(workgroup=workgroup_name))
    print("project_name: {project}".format(project=project_name))

    # Create Group
    if gitlab_action.get_group_id(workgroup_name) is None:
        gitlab_action.create_group(workgroup_name, workgroup_path)

    if gitlab_action.get_group_id(workgroup_name) is not None:
        members = gitlab_action.group_membership(workgroup_name, gitlab_user)
        print("members: {}".format(members))

    # Check if project exist
    try:
        project_id = gitlab_action.get_project_id(workgroup_name, project_name)
        print("project_id: {project}".format(project=project_id))
    except Exception as ex:
        print("ex: {ex}".format(ex=ex))

    # Create project
    if project_id is None and members is True:
        group_id = gitlab_action.get_group_id(workgroup_name)
        gitlab_action.create_project_in_group(group_id, project_name)
        project_id = gitlab_action.get_project_id(workgroup_name, project_name)
        print("project created: {prj}, {id}".format(prj=project_name, id=project_id))

    # Check Git URL, branch and depth
    git_url = gitlab_action.get_git_url()
    branch_name = gitlab_action.get_branch_name()
    git_depth = gitlab_action.get_git_depth()
    print("git_url: {}".format(git_url))
    print("branch_name: {}".format(branch_name))
    print("git_depth: {}".format(git_depth))

    # list the content of the root directory for the default branch
    my_project = gitlab_action.get_project(workgroup_name, project_name)

    # clone repository
    try:
        print("cloning repository: {repo}".format(repo=my_project.ssh_url_to_repo))
        cloned_repo = Repo.clone_from(my_project.ssh_url_to_repo, path.join(analysis_path, app_name))
        assert cloned_repo.__class__ is Repo    # clone an existing repository
    except Exception as cloned_repo_ex:
        print("cloned_repo_ex: {}".format(cloned_repo_ex))

    # Collect stages
    stages = gitlab_action.get_stages()
    print("stages: {}".format(stages))

    # Enable - auto DevOps strategy
    gitlab_action.set_auto_devops(my_project)
    print(gitlab_action.retrieve_auto_devops(my_project))
