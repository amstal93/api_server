## implementation of API Backends

### 1 API Calls
#### 1.0 API Call (/analysis/create)
```bash
[x] upload YAML
```

#### 1.1 API Call (/analysis/{abort, progress, results})
```bash
[ ] analysis results
[ ] analysis progress
[ ] analysis jobs
[ ] analysis results
```

#### 1.2 API Call (/tool/{create, remove, details, list})
```bash
[ ] create tool
[ ] remove tool
[ ] tool details
[ ] list tools
```

### 2. CI Lint
```bash
[x] lint YAML
```

local usage (with go)
```bash
go get -v gitlab.com/orobardet/gitlab-ci-linter
gitlab-ci-linter --gitlab-url "https://gitlab.local" --ci-file ".gitlab-ci.yml" check
```

local usage (with python)
```bash
python3 -m pip install -U gitlab_lint
gll --verify --domain gitlab.com --path $PWD/.gitlab-ci.yml
```

local usage (with docker)
```bash
docker run -v $PWD:/go/src/app/ ci-linter
```

### 3. GitLab API (interaction)

`python-gitlab` provides a gitlab command-line tool to interact with GitLab servers. 
It uses a configuration file to define how to connect to the servers.
To install python-gitlab do the following:
```bash
python -m pip install python-gitlab
```

The configuration file uses the INI format. 
It contains at least a [global] section, and a specific section for each GitLab server
Copy sample to `python-gitlab.cfg` and fill with your gitlab servers and tokens:
```
cp -av python-gitlab.cfg.sample python-gitlab.cfg
```

The gitlab command expects two mandatory arguments. 
The first one is the type of object that you want to manipulate. 
The second is the action that you want to perform. For example:
```
gitlab -c $PWD/python-gitlab.cfg project list
```

Use sample to create your own configuration
```
cp -av python-gitlab.cfg.sample python-gitlab.cfg
```