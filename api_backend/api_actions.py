import json
import uuid
import base64
import logging
from os import urandom, makedirs, listdir
from os.path import isdir, exists, join
from shutil import copy as shutil_copy
from shutil import rmtree
from string import Template

import yaml
from git import Repo, InvalidGitRepositoryError, GitCommandError
from private_config.variables import committer_name, committer_email, gitlab_analysis_group, analysis_path, \
    analysis_default_branch, gitlab_domain, sec_analysis_yaml, apps_path, gitlab_id, gitlab_cfg, ci_template_file, \
    ssh_cmd, gitlab_ci_yaml
from tinydb import TinyDB
from tinydb.middlewares import CachingMiddleware
from tinydb.storages import JSONStorage

from api_backend.gitlab_connector import GitlabConnector
from swagger_server.controllers.authorization_controller import check_api_key_auth


class APIActions:
    gitlab_action = None
    app_data = None
    connexion_request = None
    identity_name = None
    identity_email = None

    access_rights = 0o744
    allowed_paths = ["./", "_apps"]

    app_name = None
    app_branch = None
    app_env = None
    app_git_depth = None
    app_repo = None
    app_group_name = None
    app_dst_folder = None
    analysis_stages = None
    file_name = None

    analysis_id = None
    analysis_project = None
    analysis_path = None
    analysis_repository = None
    analysis_accepted = False

    def __init__(self, the_connexion_request, the_body):

        # commits identity
        self.identity_name = str(committer_name).title()
        self.identity_email = str(committer_email).lower()

        # referrer an remote addr
        self.connexion_request = the_connexion_request
        the_referrer = self.connexion_request.referrer
        the_remote_addr = self.connexion_request.remote_addr
        print("referrer: {}, remote_addr: {}".format(the_referrer, the_remote_addr))

        # yaml definition file
        self.app_data = yaml.safe_load(the_body)
        self.app_name = self.app_data['variables']['APP_NAME']
        self.app_branch = self.app_data['variables']['BRANCH_NAME']
        self.app_env = self.app_data['variables']['ENV']
        self.app_git_depth = self.app_data['variables']['GIT_DEPTH']
        self.app_repo = self.app_data['variables']['GIT_URL']
        self.app_group_name = self.app_data['variables']['GROUP_NAME']
        self.analysis_stages = self.app_data['stages']

        # analysis generated values
        self.analysis_id = self.generate_analysis_id()
        self.analysis_workgroup = gitlab_analysis_group
        self.analysis_project = "{app_group}-{app_name}".format(
            app_group=self.app_group_name,
            app_name=self.app_name
        )
        analysis_path_template = Template("$path/$app")
        self.analysis_path = analysis_path_template.safe_substitute(path=analysis_path, app=self.app_name)
        self.analysis_branch = analysis_default_branch
        self.analysis_repository = "git@{server}:{group}/{project}.git".format(
            server=gitlab_domain,
            group=gitlab_analysis_group,
            project=self.analysis_project
        )
        self.file_name = "{path}/{app}/{file}".format(
            path=analysis_path,
            app=self.app_name,
            file=sec_analysis_yaml
        )
        app_dst_folder_template = Template("$path/$app")
        self.app_dst_folder = app_dst_folder_template.safe_substitute(path=apps_path, app=self.app_name)

        # gitlab
        self.gitlab_action = GitlabConnector(gitlab_id, gitlab_cfg)

    def check_api_key(self):
        the_headers = self.connexion_request.headers
        api_key = the_headers['X-Api-Key']
        check_api_key_auth(api_key)

    def save_definition_file(self):
        self.create_folder(self.analysis_path, self.app_name)
        with open(self.file_name, 'w') as the_file:
            yaml.dump(self.app_data, the_file, sort_keys=False)

    def copy_ci_template_file(self):
        shutil_copy(ci_template_file, self.analysis_path)

    def create_gitlab_ci_file(self):
        self.create_folder(self.analysis_path, self.app_name)

        # open 'Security-Analysis' template and add analysis_id
        with open(ci_template_file, 'r') as template_file:
            config_data = yaml.safe_load(template_file)
        config_data['variables'] = {
            'ANALYSIS_ID': self.analysis_id,
            'ANALYSIS_WORKGROUP': self.analysis_workgroup,
            'ANALYSIS_PROJECT': self.analysis_project,
            'ANALYSIS_PATH': self.analysis_path,
            'ANALYSIS_REPO': self.analysis_repository
        }

        # write .gitlab-ci.yml to analysis folder
        ci_template = Template("$path/$filename")
        ci_filename = ci_template.safe_substitute(path=self.analysis_path, filename=gitlab_ci_yaml)
        with open(ci_filename, "w") as fh:
            yaml.dump(config_data, fh)

    def clone_repository(self, repo_url, repo_dst_folder, branch_name, debug_option=None):
        """
        :param repo_url:
        :param repo_dst_folder:
        :param branch_name:
        :param debug_option:
        :return:
        """
        repo_cloned = None
        if not repo_dst_folder.startswith(self.allowed_paths[0]) and not repo_dst_folder.startswith(self.allowed_paths[1]):
            print("path: {path} not allowed".format(path=repo_dst_folder))
            exit(1)

        # if target does not exist --> clone repo
        if exists(repo_dst_folder) is False:
            try:
                print("cloning repo to {dst_folder}".format(dst_folder=repo_dst_folder))
                the_repo = Repo.clone_from(repo_url, repo_dst_folder, env=dict(GIT_SSH_COMMAND=ssh_cmd))
                the_repo.git.checkout(branch_name)
                self.config_pull_as_rebase()
                repo_cloned = True
            except Exception as ex:
                logging.error('clone_repository error: {}'.format(ex))
            if debug_option is True:
                logging.info("repo: {}, branch: {}, downloaded".format(self.app_name, branch_name))

        # target folder exist
        else:
            # if target exists --> check branch,
            try:
                print("checkout and fetch at {dst_folder}".format(dst_folder=repo_dst_folder))
                the_repo = Repo(repo_dst_folder)
                the_repo.git.checkout(branch_name)
                repo_cloned = False
            except InvalidGitRepositoryError as ex:
                logging.error('set_repo error: {}'.format(ex))

                # not a git repo --> delete folder
                try:
                    print("delete repo_dst_folder: {dst_folder}".format(dst_folder=repo_dst_folder))
                    rmtree(repo_dst_folder)
                except OSError:
                    print("Deletion of the directory %s failed" % repo_dst_folder)
                finally:
                    repo_cloned = False

        return repo_cloned

    def rm_remote(self, the_path=None):
        if the_path is None:
            try:
                repo_obj = Repo.init(self.analysis_path)
                if repo_obj.remotes:
                    repo_obj.delete_remote('origin')
            except GitCommandError as ex1:
                print("rm_remote_ex_1: {ex}".format(ex=ex1))
        else:
            try:
                repo_obj = Repo.init(the_path)
                repo_obj.delete_remote('origin')
            except GitCommandError as ex2:
                print("rm_remote_ex_2: {ex}".format(ex=ex2))

    @staticmethod
    def check_origin(the_repo_obj, the_origin):
        assert the_origin.exists()
        assert the_origin == the_repo_obj.remotes.origin == the_repo_obj.remotes['origin']
        the_origin.fetch()  # assure we actually have data, fetch() returns useful information

    @staticmethod
    def check_master(the_repo_obj, the_origin):
        if 'master' not in the_repo_obj.heads:
            logging.debug("Creating local master branch")
            the_repo_obj.create_head(  # create local branch "master" from remote "master"
                'master',
                the_origin.refs.master
            )
        the_repo_obj.heads.master.set_tracking_branch(  # set local "master" to track remote "master"
            the_origin.refs.master
        )
        the_repo_obj.heads.master.checkout()  # checkout local "master" to working tree

    def add_remote(self, the_path=None, the_repository=None):
        if the_path is None and the_repository is None:
            repo_obj = Repo.init(self.analysis_path)
            if repo_obj.remotes and repo_obj.remotes.origin.url != the_repository:
                self.rm_remote()
            origin_obj = repo_obj.create_remote('origin', self.analysis_repository)
            self.check_origin(repo_obj, origin_obj)
        else:
            repo_obj = Repo.init(the_path)
            if repo_obj.remotes and repo_obj.remotes.origin.url != the_repository:
                self.rm_remote(the_path)
            origin_obj = repo_obj.create_remote('origin', the_repository)
            self.check_origin(repo_obj, origin_obj)

        return origin_obj

    def config_identity(self):
        repo_obj = Repo(self.analysis_path)
        with repo_obj.config_writer() as config:  # get a config writer to change configuration
            config.set_value("user", "name", self.identity_name)
            config.set_value("user", "email", self.identity_email)
            config.release()  # call release() to be sure changes are written and locks are released

    def config_pull_as_rebase(self):
        repo_obj = Repo(self.analysis_path)
        with repo_obj.config_writer() as config:  # get a config writer to change configuration
            config.set_value("pull", "rebase", "true")
            config.release()  # call release() to be sure changes are written and locks are released

    def git_push(self, the_file, the_branch, the_rebase_flag=None):
        repo_obj = Repo(self.analysis_path)
        assert not repo_obj.bare
        try:
            repo_obj.git.add(the_file)
            repo_obj.git.commit(
                '-m',
                "add file {file}".format(file=the_file),
                author="{name} <{email}>".format(name=self.identity_name, email=self.identity_email)
            )
        except GitCommandError as ex:
            print("ex: {}".format(ex))

        with repo_obj.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
            if the_rebase_flag is True:
                repo_obj.git.push('-f', 'origin', the_branch)
            else:
                repo_obj.git.push('--set-upstream', 'origin', the_branch)

    def git_push_folder(self, the_folder, the_branch):
        if isdir(the_folder) is True:
            for file in listdir(the_folder):
                self.git_push(file, the_branch)
        else:
            print("{folder} is not a directory".format(folder=the_folder))

    def git_pull(self, the_branch):
        repo_obj = Repo(self.analysis_path)
        try:
            with repo_obj.git.custom_environment(GIT_SSH_COMMAND=ssh_cmd):
                repo_obj.git.fetch()
                repo_obj.git.pull('origin', the_branch)
            return True
        except GitCommandError as ex:
            print("ex: {}".format(ex))
            return False

    @staticmethod
    def print_commit(the_commit):
        print('----')
        print(str(the_commit.hexsha))
        print("\"{summary}\" by {name} ({email})".format(
            summary=the_commit.summary,
            name=the_commit.author.name,
            email=the_commit.author.email)
        )
        print(str(the_commit.authored_datetime))
        print(str("count: {count} and size: {size}".format(
            count=the_commit.count(),
            size=the_commit.size))
        )

    @staticmethod
    def setup_db():
        return TinyDB('./bd/backend_db.json', storage=CachingMiddleware(JSONStorage))

    def create_folder(self, the_path, the_folder):
        if not the_path.startswith(self.allowed_paths[0]) and not the_path.startswith(self.allowed_paths[1]):
            print("path: {path} not allowed".format(path=the_path))
            exit(1)

        folder_template = Template("$path/$folder_name")
        folder_location = folder_template.safe_substitute(path=the_path, folder_name=the_folder)

        if isdir(folder_location) is False:
            try:
                makedirs(folder_location, self.access_rights)
            except OSError:
                print("Creation of the directory {folder} failed".format(folder=folder_location))

    @staticmethod
    def generate_uuid():
        return uuid.uuid1()

    @staticmethod
    def generate_analysis_id():
        return base64.b64encode(urandom(48)).decode('UTF-8').strip()

    # POST /analysis/create
    def create_analysis(self):
        """
            steps

            0. check api_key                # done
            1. clone repo                   # done
            2. lint analysis definition     # done
            3. create workgroup and project # done

            4. setup analysis                                       # done
                4.1 generate analysis id                            # done

                4.2 clone remote analysis repository                # done
                4.4 config remote                                   # done
                4.6 fetch/rebase or pull                            # done
                4.7 create gitlab-ci.yml                            # done
                4.8 include security-analysis on gitlab-ci.yml      # done
                4.9 create analysis folder                          # done
                4.10 save security-analysis.yml                     # done
                4.11 config committer identity (name,email)         # done

                4.12 add, commit .gitlab-ci file                    # done
                4.13 push .gitlab-ci file                           # done

            5. collect stages                                       # done
            6. enable auto devops

            7. run sec tools
            7.1 secret detection
            7.2 static analysis
            7.2 dynamic analysis
        """

        # 1. clone application repository
        self.clone_repository(self.app_repo, self.app_dst_folder, self.app_branch)

        # 2. lint - analysis definition file (yaml)
        ci_definition = json.dumps(self.app_data)
        success, errors = self.gitlab_action.lint_yaml(ci_definition)
        print(success, errors)

        # 3. create workgroup and project
        self.gitlab_action.create_workgroup_and_project(self.analysis_workgroup, self.analysis_project)

        # 4. setup analysis
        self.analysis_id = self.generate_analysis_id()
        self.clone_repository(self.analysis_repository, self.analysis_path, self.analysis_branch)
        origin = self.add_remote()
        rebase_flag = self.git_pull(self.analysis_branch)
        self.save_definition_file()
        self.create_gitlab_ci_file()
        self.config_identity()

        if origin is not None:
            self.git_push(gitlab_ci_yaml, self.analysis_branch, rebase_flag)
            self.git_push(sec_analysis_yaml, self.analysis_branch, rebase_flag)

            # TODO: check if pipeline started
            self.analysis_accepted = True

        # 5. Collect stages
        stages = self.analysis_stages
        print("stages: {}".format(stages))

        # Enable - auto DevOps strategy
        project_obj = self.gitlab_action.get_project(self.analysis_workgroup, self.analysis_project)
        self.gitlab_action.set_auto_devops(project_obj)
        assert self.gitlab_action.retrieve_auto_devops(project_obj) is True

        return {
            "analysis_id": self.analysis_id,
            "analysis_project": self.analysis_project,
            'accepted': "true" if self.analysis_accepted is True else "false",
            'filename': self.file_name,
        }
