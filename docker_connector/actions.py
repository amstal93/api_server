import docker
import logging
import multiprocessing


class DockerActions:
    """
        https://docs.docker.com/engine/api/v1.40/
        https://docs.docker.com/engine/api/sdk/examples/
        https://docker-py.readthedocs.io/en/stable/containers.html
        https://docker-py.readthedocs.io/en/stable/api.html#module-docker.api.exec_api
        https://github.com/AlexIoannides/py-docker-aws-example-project/blob/master/deploy_to_aws.py
    """

    client = None

    def __init__(self):
        self.client = docker.from_env()

    def run_container(self, image_name, image_tag="latest", command=None, run_parameters=None):
        """
        Run a container 🔗
        @param image_name: string
        @param image_tag: string
        @param command: string
        @param run_parameters: string
        @return: container_id, image_id
        """

        try:
            image_obj = self.check_image(image_name, image_tag)
        except Exception as ex:
            logging.error("run_container() error: {}".format(ex))
            image_obj = self.pull_image(image_name, image_tag)

        image = image_name + ':' + image_tag
        container_obj = self.client.containers.run(image, command=command)
        results = {"container_obj": container_obj, "image_obj": image_obj}
        logging("container_obj: {}".format(container_obj))

        return results

    def pull_image(self, image_name, image_tag="latest"):
        """
        Pull a image 🔗
        @param image_tag:
        @param image_name:
        @return: image_id
        """
        image = self.client.images.pull(image_name + ":" + image_tag)
        return image.id

    def list_images(self):
        local_images = self.client.images.list()
        logging.info("local images: {}".format(local_images))

        return local_images

    def check_image(self, image_name, image_tag="latest"):

        local_images = self.list_images()
        for image in local_images:
            if image_name + ':' + image_tag in image.attrs['RepoTags']:
                print('image {} found'.format(image_name + ':' + image_tag))
                return True

    def build_image(self, dockerfile_path, dockerfile_name, image_name, image_tag="latest", rm_intermediate=True):
        image, build_log = self.client.images.build(path=dockerfile_path, dockerfile=dockerfile_name , tag=image_name + ':' + image_tag, rm=rm_intermediate)
        logging.info("image: {}, build_log{}".format(image, build_log))

    @staticmethod
    def stop_container(container_id, args=None):
        """
        Stop a container 🔗
        @param container_id:
        @param args:
        """
        container_id.stop()

    def list_containers(self):
        for container in self.client.containers.list():
            print("container: {}".format(container))

    def stop_all(self):
        for container in self.client.containers.list():
            container.stop()

    def prune(self):
        print(self.client.containers.prune())

    @staticmethod
    def collect_logs(container_id):
        """
        Logs of a specific container🔗
        """
        return container_id.logs().decode('utf-8')

    def launch_in_parallel(self, containers_to_launch):

        index = 0
        pool_size = multiprocessing.cpu_count()
        the_pool = multiprocessing.Pool(pool_size)
        parallel_work = tuple()

        for ctr in containers_to_launch:
            image_name = ctr['image']['image_name']
            tag = ctr['image']['image_tag']
            command = ctr['image']['command']
            new_job = [image_name, tag, command, None]
            logging.info('idx {}, image_name {}, tag {}, command: {} ...'.format(index, image_name, tag, command))

            if len(parallel_work) < pool_size:
                parallel_work += (new_job,)

            if len(parallel_work) == pool_size:
                logging.info('launching {} containers'.format(pool_size))
                the_pool.map(self.run_container, parallel_work)
                parallel_work = ()

            index += 1

    def launch_in_series(self, containers_to_launch):

        index = 0

        for ctr in containers_to_launch:
            for k, values in ctr.items():
                logging.info('launching {}, idx {} ...'.format(k, index))

                image_name = ctr['image']['image_name']
                tag = ctr['image']['image_tag']
                command = ctr['image']['command']

                self.run_container(image_name, tag, command, None)

            index += 1

    def list_networks(self):
        all_networks = [n.name for n in self.client.networks.list()]
        print(all_networks)
        return all_networks
